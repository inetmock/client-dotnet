using System.IO;
using INetMock.Client.Audit;
using INetMock.Client.Audit.Serialization;
using INetMock.Client.Test.Hex;
using Xunit;

namespace INetMock.Client.Test.Audit.Serialization;

public class GenericReaderTest
{
    private const string HttpEventPayload = "000000a7120b088092b8c398feffffff01180120022a047f00000132047f00000138d8fc0140504a3308041224544c535f45434448455f45434453415f574954485f4145535f3235365f4342435f5348411a096c6f63616c686f7374a2014c080112096c6f63616c686f73741a15687474703a2f2f6c6f63616c686f73742f6173646622084854545020312e312a1c0a0641636365707412120a106170706c69636174696f6e2f6a736f6e";
    private const string DnsEventPayload = "0000004e120b088092b8c398feffffff01180220012a100000000000000000000000000000000132100000000000000000000000000000000138d8fc014050aa0110120e0801120a6769746c61622e636f6d";

    private readonly byte[] _httpEventPayloadBytes;
    private readonly byte[] _dnsEventPayloadBytes;

    public GenericReaderTest()
    {
        _httpEventPayloadBytes = HttpEventPayload.HexToByteArray();
        _dnsEventPayloadBytes = DnsEventPayload.HexToByteArray();
    }

    [Fact]
    public async void TestRead_HttpEvent_Success()
    {
        await using var protoReader = new ProtoReader(new MemoryStream(_httpEventPayloadBytes));
        await using IEventReader reader = new GenericReader(protoReader);
        await foreach (var ev in reader.ReadAllAsync())
        {
            Assert.NotNull(ev);
            Assert.NotNull(ev.Details);

            Assert.NotNull(ev.DetailsAs<HttpDetails>());
            Assert.Null(ev.DetailsAs<DnsDetails>());

            var httpEvent = (Event<HttpDetails>)ev;
            Assert.NotNull(httpEvent);
            Assert.NotNull(httpEvent.Details);
        }
    }

    [Fact]
    public async void TestRead_DnsEvent_Success()
    {
        await using var memStream = new MemoryStream(_dnsEventPayloadBytes);
        await using var protoReader = new ProtoReader(new MemoryStream(_dnsEventPayloadBytes));
        await using IEventReader reader = new GenericReader(protoReader);
        await foreach (var ev in reader.ReadAllAsync())
        {
            Assert.NotNull(ev);
            Assert.NotNull(ev.Details);
            Assert.NotNull(ev.DetailsAs<DnsDetails>());

            var dnsEvent = (Event<DnsDetails>)ev;
            Assert.NotNull(dnsEvent);
            Assert.NotNull(dnsEvent.Details);
        }
    }
}

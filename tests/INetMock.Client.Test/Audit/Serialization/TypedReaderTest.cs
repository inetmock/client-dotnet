using System.IO;
using INetMock.Client.Audit;
using INetMock.Client.Audit.Serialization;
using INetMock.Client.Test.Hex;
using Xunit;

namespace INetMock.Client.Test.Audit.Serialization;

public class TypedReaderTest
{
    private const string HttpEventPayload = "000000e5120b088092b8c398feffffff01180120022a047f00000132047f00000138d8fc0140504a3308041224544c535f45434448455f45434453415f574954485f4145535f3235365f4342435f5348411a096c6f63616c686f7374528a010a3c747970652e676f6f676c65617069732e636f6d2f696e65746d6f636b2e61756469742e64657461696c732e4854545044657461696c73456e74697479124a12096c6f63616c686f73741a15687474703a2f2f6c6f63616c686f73742f6173646622084854545020312e312a1c0a0641636365707412120a106170706c69636174696f6e2f6a736f6e";
    private const string DnsEventPayload = "0000003b120b088092b8c398feffffff01180120012a100000000000000000000000000000000132100000000000000000000000000000000138d8fc014050";

    private readonly byte[] _httpEventPayloadBytes;
    private readonly byte[] _dnsEventPayloadBytes;

    public TypedReaderTest()
    {
        _httpEventPayloadBytes = HttpEventPayload.HexToByteArray();
        _dnsEventPayloadBytes = DnsEventPayload.HexToByteArray();
    }

    [Fact]
    public async void TestRead_HttpEvent_Success()
    {
        await using var protoReader = new ProtoReader(new MemoryStream(_httpEventPayloadBytes));
        await using IEventReader<HttpDetails> reader = new TypedReader<HttpDetails>(protoReader, DropMode.DropEntity);
        await foreach (var ev in reader.ReadAllAsync())
        {
            Assert.NotNull(ev);
        }
    }

    [Fact]
    public async void TestRead_DnsEvent_Success()
    {
        await using var protoReader = new ProtoReader(new MemoryStream(_dnsEventPayloadBytes));
        await using IEventReader<DnsDetails> reader = new TypedReader<DnsDetails>(protoReader, DropMode.DropEntity);
        await foreach (var ev in reader.ReadAllAsync())
        {
            Assert.NotNull(ev);
        }
    }
}

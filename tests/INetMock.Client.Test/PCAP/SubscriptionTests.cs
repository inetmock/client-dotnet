using INetMock.Client.PCAP;
using Xunit;

namespace INetMock.Client.Test.PCAP;

public class SubscriptionTests
{
    [Theory]
    [InlineData("lo:test.pcap", "test.pcap", "lo")]
    public void Constructor_ConsumerKey_SplitIntoComponents(string key, string expectedName, string expectedDevice)
    {
        var sub = new Subscription(key);

        Assert.Equal(sub, new Subscription(key, expectedName, expectedDevice));
    }
}

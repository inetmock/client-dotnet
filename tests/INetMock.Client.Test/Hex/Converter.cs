using System;
using System.Linq;

namespace INetMock.Client.Test.Hex;

public static class Converter
{
    public static byte[] HexToByteArray(this string hex)
    {
        return Enumerable.Range(0, hex.Length)
            .Where(x => x % 2 == 0)
            .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
            .ToArray();
    }
}

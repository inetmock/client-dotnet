using System.IO;
using System.Threading.Tasks;
using INetMock.Client.Audit;
using INetMock.Client.Audit.Serialization;
using Xunit;

namespace INetMock.Client.IntegrationTest.Audit.Serialization;

public class GenericReaderTest
{
    [Theory]
    [InlineData("test.ima")]
    [InlineData("test2.ima")]
    [InlineData("test3.ima")]
    public async Task Test_ReadAllAsync_AuditFile(string testFile)
    {
        await using var auditFileStream = File.OpenRead(Path.Join("testdata", testFile));
        await using IEventReader reader = new GenericReader(new ProtoReader(auditFileStream));

        var count = 0;
        await foreach (var ev in reader.ReadAllAsync())
        {
            Assert.NotNull(ev);
            count++;
        }

        Assert.True(count > 0);
    }
}

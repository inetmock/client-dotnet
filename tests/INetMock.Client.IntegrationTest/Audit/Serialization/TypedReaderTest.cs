using System.IO;
using System.Threading.Tasks;
using INetMock.Client.Audit;
using INetMock.Client.Audit.Serialization;
using Xunit;

namespace INetMock.Client.IntegrationTest.Audit.Serialization;

public class TypedReaderTest
{
    [Fact]
    public async Task Test_ReadAllAsync_DropDetails_AuditFile_HTTPEvents()
    {
        await using var fileStream = File.OpenRead(Path.Join("testdata", "test.ima"));
        await using IEventReader<HttpDetails> httpReader = new TypedReader<HttpDetails>(new ProtoReader(fileStream));

        var count = 0;
        await foreach (var ev in httpReader.ReadAllAsync())
        {
            Assert.NotNull(ev);
            count++;
        }
        Assert.True(count > 0);
    }

    [Fact]
    public async Task Test_ReadAllAsync_DropEntity_AuditFile_HTTPEvents()
    {
        await using var fileStream = File.OpenRead(Path.Join("testdata", "test.ima"));
        await using IEventReader<HttpDetails> httpReader = new TypedReader<HttpDetails>(new ProtoReader(fileStream), DropMode.DropEntity);

        var count = 0;
        await foreach (var ev in httpReader.ReadAllAsync())
        {
            Assert.NotNull(ev);
            Assert.NotNull(ev.Details);
            count++;
        }
        Assert.True(count > 0);
    }

    [Fact]
    public async Task Test_ReadAllAsync_DropDetails_AuditFile_DNSEvents()
    {
        await using var fileStream = File.OpenRead(Path.Join("testdata", "test.ima"));
        await using IEventReader<DnsDetails> httpReader = new TypedReader<DnsDetails>(new ProtoReader(fileStream));

        var count = 0;
        await foreach (var ev in httpReader.ReadAllAsync())
        {
            Assert.NotNull(ev);
            count++;
        }
        Assert.True(count > 0);
    }

    [Fact]
    public async Task Test_ReadAllAsync_DropEntity_AuditFile_DNSEvents()
    {
        await using var fileStream = File.OpenRead(Path.Join("testdata", "test.ima"));
        await using IEventReader<DnsDetails> httpReader = new TypedReader<DnsDetails>(new ProtoReader(fileStream), DropMode.DropEntity);

        var count = 0;
        await foreach (var ev in httpReader.ReadAllAsync())
        {
            Assert.NotNull(ev);
            count++;
        }
        Assert.True(count > 0);
    }
}

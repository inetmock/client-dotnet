using System;

namespace INetMock.Client.IntegrationTest;

public class INetMockFixture
{
    private const string DefaultINetMockSocketPath = "unix:///var/run/inetmock/inetmock.sock";

    public INetMockFixture()
    {
        INetMockSocketPath = Environment.GetEnvironmentVariable("INETMOCK_SOCKET") ?? DefaultINetMockSocketPath;
        if (INetMockSocketPath.StartsWith("http:"))
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
        }
    }

    public string INetMockSocketPath { get; }
}

using System.Linq;
using INetMock.Client.PCAP;
using INetMock.Client.PCAP.Client;
using Xunit;
using Xunit.Abstractions;

namespace INetMock.Client.IntegrationTest.PCAP.Client;

public class PcapApiClientTests : IClassFixture<INetMockFixture>
{
    private readonly ITestOutputHelper _outputHelper;
    private readonly IPcapApiClient _apiClient;

    public PcapApiClientTests(ITestOutputHelper testOutputHelper, INetMockFixture inetMockFixture)
    {
        _outputHelper = testOutputHelper;
        _apiClient = new PcapApiClient(inetMockFixture.INetMockSocketPath);
    }

    [Fact]
    public async void ListAvailableDevicesAsync_RunningContainer_AtLeastLoopbackDevice()
    {
        var devs = await _apiClient.ListAvailableDevicesAsync();

        foreach (var (name, _) in devs)
        {
            _outputHelper.WriteLine(name);
        }

        Assert.Contains(devs, device => device.Name.Equals("lo"));
    }

    [Fact]
    public async void ListActiveRecordingsAsync_NoRecordingsRunning_EmptyResult()
    {
        var recordings = await _apiClient.ListActiveRecordingsAsync();

        Assert.Empty(recordings);
    }

    [Fact]
    public async void StartPcapFileRecordingAsync_RecordLoopbackInterface_RunningRecording()
    {
        var recordingDevice = (await _apiClient.ListAvailableDevicesAsync()).FirstOrDefault();
        if (recordingDevice == null)
        {
            return;
        }

        var targetPath = $"/tmp/{recordingDevice.Name}_record.pcap";
        await _apiClient.StartPcapFileRecordingAsync(new(recordingDevice.Name, targetPath));

        var subscriptions = await _apiClient.ListActiveRecordingsAsync();
        Assert.Contains(subscriptions, subscription =>
            subscription.Device.Equals(recordingDevice.Name) &&
            subscription.ConsumerName.Equals(targetPath)
        );
        foreach (var subscription in subscriptions)
        {
            await _apiClient.StopPcapFileRecordingAsync(subscription.ConsumerKey);
        }
    }
}

using System;
using System.Net.Http;
using System.Net.Sockets;
using Grpc.Net.Client;

namespace INetMock.Client.Grpc;

internal static class ChannelFactory
{
    internal static GrpcChannel ForAddress(Uri uri, GrpcChannelOptions options) =>
        uri.Scheme.ToLowerInvariant() switch
        {
            "unix" => ForUnixSocket(uri.AbsolutePath, options),
            _ => GrpcChannel.ForAddress(uri, options)
        };

    private static GrpcChannel ForUnixSocket(string path, GrpcChannelOptions options)
    {
        var endpoint = new UnixDomainSocketEndPoint(path);
        options.HttpHandler = new SocketsHttpHandler
        {
            ConnectCallback = async (_, cancellationToken) =>
            {
                var socket = new Socket(AddressFamily.Unix, SocketType.Stream, ProtocolType.Unspecified);
                try
                {
                    await socket.ConnectAsync(endpoint, cancellationToken).ConfigureAwait(false);
                    return new NetworkStream(socket, true);
                }
                catch
                {
                    socket.Dispose();
                    throw;
                }
            }
        };
        return GrpcChannel.ForAddress("http://localhost", options);
    }
}

using System;
using System.Collections.Generic;
using System.Net;

namespace INetMock.Client.PCAP;

public record RecordingDevice(string Name, IReadOnlyList<IPAddress> Addresses);

public record Subscription
{
    public Subscription(string consumerKey)
    {
        ConsumerKey = consumerKey;
        (ConsumerName, Device) = SplitConsumerKey(consumerKey);
    }

    public Subscription(string consumerKey, string consumerName, string device)
    {
        ConsumerKey = consumerKey;
        ConsumerName = consumerName;
        Device = device;
    }

    public string ConsumerKey { get; init; }
    public string ConsumerName { get; init; }
    public string Device { get; init; }

    private static (string name, string key) SplitConsumerKey(string consumerKey)
    {
        var splitIndex = consumerKey.IndexOf(':');
        if (splitIndex < 0)
        {
            throw new ArgumentOutOfRangeException(
                nameof(consumerKey),
                "The given consumer key could not be split into components"
            );
        }

        return (consumerKey[(splitIndex + 1)..], consumerKey[..splitIndex]);
    }
}

public record RecordingRequest(string Device, string TargetPath, bool Promiscuous = false)
{
    public RecordingRequest(string device, string targetPath, bool promiscuous, TimeSpan readTimeout) : this(device,
        targetPath, promiscuous)
    {
        ReadTimeout = readTimeout;
    }

    public TimeSpan ReadTimeout { get; } = TimeSpan.FromSeconds(30);
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;
using ChannelFactory = INetMock.Client.Grpc.ChannelFactory;

namespace INetMock.Client.PCAP.Client;

public class PcapApiClient : IPcapApiClient
{
    private readonly PCAPService.PCAPServiceClient _pcapServiceClient;

    public PcapApiClient(string address, GrpcChannelOptions? options = null)
        : this(new Uri(address), options)
    {
    }

    public PcapApiClient(Uri address, GrpcChannelOptions? options = null)
        : this(ChannelFactory.ForAddress(address, options ?? new GrpcChannelOptions()))
    {
    }

    public PcapApiClient(ChannelBase channel)
    {
        _pcapServiceClient = new PCAPService.PCAPServiceClient(channel);
    }

    public async Task<IReadOnlyList<RecordingDevice>> ListAvailableDevicesAsync(CancellationToken token = default)
    {
        var devices = await _pcapServiceClient.ListAvailableDevicesAsync(new(), Metadata.Empty, null, token);
        return devices.AvailableDevices
            .Select(d => new RecordingDevice(
                    d.Name,
                    d.Addresses
                        .Select(addr => new IPAddress(addr.Span))
                        .ToList()
                )
            )
            .ToList();
    }

    public async Task<IReadOnlyList<Subscription>> ListActiveRecordingsAsync(CancellationToken token = default)
    {
        var recordings = await _pcapServiceClient.ListActiveRecordingsAsync(new(), Metadata.Empty, null, token);
        return recordings.Subscriptions
            .Select(consumerKey => new Subscription(consumerKey))
            .ToList();
    }

    public async Task<string> StartPcapFileRecordingAsync(RecordingRequest request,
        CancellationToken token = default)
    {
        var clientRequest = new StartPCAPFileRecordingRequest
        {
            Device = request.Device,
            Promiscuous = request.Promiscuous,
            TargetPath = request.TargetPath,
            ReadTimeout = Duration.FromTimeSpan(request.ReadTimeout)
        };

        var result = await _pcapServiceClient.StartPCAPFileRecordingAsync(
            clientRequest,
            Metadata.Empty,
            null,
            token);

        return result.ResolvedPath;
    }

    public async Task<bool> StopPcapFileRecordingAsync(string consumerKey, CancellationToken token = default)
    {
        var clientRequest = new StopPCAPFileRecordingRequest { ConsumerKey = consumerKey };
        var result = await _pcapServiceClient.StopPCAPFileRecordingAsync(
            clientRequest,
            Metadata.Empty,
            null,
            token
        );
        return result.Removed;
    }
}

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace INetMock.Client.PCAP;

public interface IPcapApiClient
{
    Task<IReadOnlyList<RecordingDevice>> ListAvailableDevicesAsync(CancellationToken token = default);
    Task<IReadOnlyList<Subscription>> ListActiveRecordingsAsync(CancellationToken token = default);
    Task<string> StartPcapFileRecordingAsync(RecordingRequest request, CancellationToken token = default);
    Task<bool> StopPcapFileRecordingAsync(string consumerKey, CancellationToken token = default);
}

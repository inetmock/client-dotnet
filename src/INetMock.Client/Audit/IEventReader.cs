using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace INetMock.Client.Audit;

public interface IEventReader : IDisposable, IAsyncDisposable
{
    IAsyncEnumerable<Event> ReadAllAsync(CancellationToken token = default);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="token"></param>
    /// <returns>
    ///     An event as long as underlying stream has data.
    ///     When the end of the stream has been reached it will return null.
    /// </returns>
    Task<Event?> ReadAsync(CancellationToken token = default);
}

public interface IEventReader<T> : IDisposable, IAsyncDisposable where T : EventDetails, new()
{
    IAsyncEnumerable<Event<T>> ReadAllAsync(CancellationToken token = default);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="token"></param>
    /// <returns>
    ///     An event as long as underlying stream has data.
    ///     When the end of the stream has been reached it will return null.
    /// </returns>
    Task<Event<T>?> ReadAsync(CancellationToken token = default);
}

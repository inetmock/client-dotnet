using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;
using INetMock.Client.Audit.Serialization;
using INetMock.Client.Grpc;

namespace INetMock.Client.Audit.Client;

public class AuditApiClient : IAuditApiClient
{
    private readonly AuditService.AuditServiceClient _auditClient;

    public AuditApiClient(string address, GrpcChannelOptions? options = null) : this(new Uri(address), options)
    {
    }

    public AuditApiClient(Uri address, GrpcChannelOptions? options = null) : this(
        ChannelFactory.ForAddress(address, options ?? new GrpcChannelOptions()))
    {
    }

    public AuditApiClient(ChannelBase channel)
    {
        _auditClient = new AuditService.AuditServiceClient(channel);
    }

    public async Task<IReadOnlyList<string>> ListSinksAsync(CancellationToken token = default)
    {
        var sinks = await _auditClient.ListSinksAsync(new ListSinksRequest(), Metadata.Empty, null, token);
        return sinks.Sinks;
    }

    public async Task<string> RegisterFileSinkAsync(string targetPath, CancellationToken token = default)
    {
        var resp = await _auditClient.RegisterFileSinkAsync(
            new RegisterFileSinkRequest { TargetPath = targetPath },
            Metadata.Empty,
            null,
            token
        );

        return resp.ResolvedPath;
    }

    public async Task<bool> RemoveFileSinkAsync(string targetPath, CancellationToken token = default)
    {
        var resp = await _auditClient.RemoveFileSinkAsync(new RemoveFileSinkRequest
        {
            TargetPath = targetPath
        },
            Metadata.Empty,
            null,
            token
        );

        return resp.SinkGotRemoved;
    }

    public IProtoEventReader EventStreamAsync(string watcherName, CancellationToken token = default)
    {
        var stream = _auditClient.WatchEvents(
            new WatchEventsRequest { WatcherName = watcherName },
            Metadata.Empty,
            null,
            token
        );
        return new EventServerStreamReader(stream);
    }
}

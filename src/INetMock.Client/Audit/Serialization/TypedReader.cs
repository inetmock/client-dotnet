using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace INetMock.Client.Audit.Serialization;

/// <summary>
///     Configures how the TypedReader proceeds with mismatching entities.
///     Given a TypedReader&lt;HttpDetails&gt; it can't guarantee that every Event&lt;T&gt; contains a HttpDetails entity.
///     Therefore different dropping strategies can be configured.
/// </summary>
public enum DropMode
{
    /// <summary>
    ///     Drops the whole entity.
    ///     This filters the actual stream for entities supporting the details in question.
    /// </summary>
    DropEntity,

    /// <summary>
    ///     Drops only the mismatching details but keeps the event
    ///     e.g. a TypedReader&lt;HttpDetails&gt; would still contain DNS events but no details about them
    /// </summary>
    DropDetails
}

public sealed class TypedReader<T> : IEventReader<T> where T : EventDetails, new()
{
    private readonly IProtoEventReader _reader;
    private readonly DropMode _dropMode;

    public TypedReader(IProtoEventReader reader, DropMode dropMode = DropMode.DropDetails)
    {
        _reader = reader;
        _dropMode = dropMode;
    }

    public async IAsyncEnumerable<Event<T>> ReadAllAsync([EnumeratorCancellation] CancellationToken token = default)
    {
        while (true)
        {
            var ev = await ReadAsync(token);
            if (ev == null)
            {
                yield break;
            }

            yield return ev;
        }
    }

    public async Task<Event<T>?> ReadAsync(CancellationToken token = default)
    {
        do
        {
            var entity = await _reader.ReadAsync(token);
            if (entity == null)
            {
                return null;
            }

            var parsed = new Event<T>(entity);

            var canReturn = (_dropMode, parsed.Details) switch
            {
                (DropMode.DropDetails, null) => true,
                (DropMode.DropEntity, null) => false,
                (_, _) => true
            };

            if (canReturn)
            {
                return parsed;
            }
        } while (true);
    }

    public void Dispose() => _reader.Dispose();

    public ValueTask DisposeAsync() => _reader.DisposeAsync();
}

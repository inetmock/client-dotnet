using System.Buffers;
using System.Buffers.Binary;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf;

namespace INetMock.Client.Audit.Serialization;

public sealed class ProtoReader : IProtoEventReader
{
    private readonly MemoryPool<byte> _memoryPool;
    private readonly Stream _sourceStream;
    private readonly bool _keepStreamOpen;

    public ProtoReader(Stream sourceStream, bool keepStreamOpen = false)
    {
        _memoryPool = MemoryPool<byte>.Shared;
        _sourceStream = sourceStream;
        _keepStreamOpen = keepStreamOpen;
    }

    public async Task<EventEntity?> ReadAsync(CancellationToken token = default)
    {
        using var rentedLengthMem = _memoryPool.Rent(4);
        var lengthMem = rentedLengthMem.Memory.Slice(0, 4);
        var read = await _sourceStream.ReadAsync(lengthMem, token);
        if (read != 4)
        {
            return null;
        }

        var messageLength = BinaryPrimitives.ReadInt32BigEndian(lengthMem.Span);
        using var rentedMsgMem = _memoryPool.Rent(messageLength);
        var msgMem = rentedMsgMem.Memory.Slice(0, messageLength);
        read = await _sourceStream.ReadAsync(msgMem, token);
        if (read != messageLength)
        {
            return null;
        }

        var entity = new EventEntity();
        entity.MergeFrom(msgMem.ToArray());

        return entity;
    }

    public async ValueTask DisposeAsync()
    {
        if (!_keepStreamOpen)
        {
            await _sourceStream.DisposeAsync();
        }

        _memoryPool.Dispose();
    }

    public void Dispose()
    {
        if (!_keepStreamOpen)
        {
            _sourceStream?.Dispose();
        }

        _memoryPool?.Dispose();
    }
}

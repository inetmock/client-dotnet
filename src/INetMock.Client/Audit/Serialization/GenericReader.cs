using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace INetMock.Client.Audit.Serialization;

public sealed class GenericReader : IEventReader
{
    private readonly IProtoEventReader _reader;

    public GenericReader(IProtoEventReader reader)
    {
        _reader = reader;
    }

    public async IAsyncEnumerable<Event> ReadAllAsync(
        [EnumeratorCancellation] CancellationToken token = default)
    {
        while (true)
        {
            var ev = await ReadAsync(token);
            if (ev == null)
            {
                yield break;
            }

            yield return ev;
        }
    }

    public async Task<Event?> ReadAsync(CancellationToken token = default)
    {
        var entity = await _reader.ReadAsync(token);
        return entity == null ? null : new Event(entity);
    }

    public ValueTask DisposeAsync() => _reader.DisposeAsync();

    public void Dispose()
    {
        _reader.Dispose();
    }
}

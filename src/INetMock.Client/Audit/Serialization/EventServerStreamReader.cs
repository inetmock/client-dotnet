using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;

namespace INetMock.Client.Audit.Serialization;

public sealed class EventServerStreamReader : IProtoEventReader
{
    private readonly AsyncServerStreamingCall<WatchEventsResponse> _asyncEventStream;

    public EventServerStreamReader(AsyncServerStreamingCall<WatchEventsResponse> asyncEventStream)
    {
        _asyncEventStream = asyncEventStream;
    }

    public async Task<EventEntity?> ReadAsync(CancellationToken token = default)
    {
        if (!await _asyncEventStream.ResponseStream.MoveNext(token))
        {
            return null;
        }

        return _asyncEventStream.ResponseStream.Current.Entity;
    }

    public void Dispose() => _asyncEventStream.Dispose();

    public ValueTask DisposeAsync()
    {
        _asyncEventStream.Dispose();
        return ValueTask.CompletedTask;
    }
}

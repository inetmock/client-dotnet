using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace INetMock.Client.Audit;

public interface IAuditApiClient
{
    IProtoEventReader EventStreamAsync(string watcherName, CancellationToken token = default);
    Task<IReadOnlyList<string>> ListSinksAsync(CancellationToken token = default);
    Task<string> RegisterFileSinkAsync(string targetPath, CancellationToken token = default);
    Task<bool> RemoveFileSinkAsync(string targetPath, CancellationToken token = default);
}

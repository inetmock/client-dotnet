using System.Net.Http.Headers;
using Google.Protobuf.Collections;

namespace INetMock.Client.Audit;

internal class INetMockHttpHeaders : HttpHeaders
{
    internal INetMockHttpHeaders(MapField<string, HTTPHeaderValue> headers)
    {
        foreach (var (key, values) in headers)
        {
            if (string.IsNullOrEmpty(key) || values == null)
            {
                continue;
            }

            Add(key, values.Values);
        }
    }
}

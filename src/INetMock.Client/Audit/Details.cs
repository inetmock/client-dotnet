using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Google.Protobuf.Collections;

namespace INetMock.Client.Audit;

public abstract record EventDetails;

public record HttpDetails() : EventDetails
{
    public HttpDetails(HTTPDetailsEntity detailsEntity) : this()
    {
        Method = new HttpMethod(detailsEntity.Method.ToString());
        Host = detailsEntity.Host;
        Uri = detailsEntity.Uri;
        Proto = detailsEntity.Proto;
        Headers = new INetMockHttpHeaders(detailsEntity.Headers);
    }

    public HttpMethod Method { get; init; } = HttpMethod.Get;
    public string Host { get; init; } = string.Empty;
    public string Uri { get; init; } = string.Empty;
    public string Proto { get; init; } = string.Empty;
    public HttpHeaders Headers { get; init; } = new INetMockHttpHeaders(new MapField<string, HTTPHeaderValue>());
}

public record DnsDetails() : EventDetails
{
    public DnsDetails(DNSDetailsEntity entity) : this()
    {
        OpCode = entity.Opcode;
        Questions = entity.Questions;
    }

    public DNSOpCode OpCode { get; init; } = DNSOpCode.Query;
    public IReadOnlyList<DNSQuestionEntity> Questions { get; init; } = Array.Empty<DNSQuestionEntity>();
}

public record DhcpDetails() : EventDetails
{
    public DhcpDetails(DHCPDetailsEntity entity) : this()
    {
        HopCount = entity.HopCount;
        OpCode = entity.Opcode;
        HardwareType = entity.HwType;
    }

    public int HopCount { get; init; }
    public DHCPOpCode OpCode { get; init; } = DHCPOpCode.Unspecified;
    public DHCPHwType HardwareType { get; init; } = DHCPHwType.Unspecified;
}

public record NetMonDetails() : EventDetails
{
    public NetMonDetails(NetMonDetailsEntity entity) : this()
    {
        ReverseResolvedHost = entity.ReverseResolvedHost;
    }

    public string ReverseResolvedHost { get; init; } = string.Empty;
}

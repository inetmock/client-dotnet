using System;
using System.Net;

namespace INetMock.Client.Audit;

public abstract record EventBase()
{
    protected EventBase(EventEntity entity) : this()
    {
        if (entity == null)
        {
            throw new ArgumentNullException(nameof(entity));
        }

        Id = entity.Id;
        Timestamp = entity.Timestamp.ToDateTimeOffset();
        Transport = entity.Transport;
        SourceIp = new IPAddress(entity.SourceIp.Span);
        SourcePort = Convert.ToUInt16(entity.SourcePort);
        DestinationIp = new IPAddress(entity.DestinationIp.Span);
        DestinationPort = Convert.ToUInt16(entity.DestinationPort);
        TlsDetails = entity.Tls;
        Application = entity.Application;
    }

    public long Id { get; init; }

    public DateTimeOffset Timestamp { get; init; } = DateTimeOffset.UnixEpoch;

    public TransportProtocol Transport { get; init; } = TransportProtocol.Unspecified;

    public AppProtocol Application { get; init; } = AppProtocol.Unspecified;

    public IPAddress SourceIp { get; init; } = IPAddress.Any;

    public IPAddress DestinationIp { get; init; } = IPAddress.Any;

    public ushort SourcePort { get; init; }

    public ushort DestinationPort { get; init; }

    public TLSDetailsEntity? TlsDetails { get; init; }

    public bool IsTls => TlsDetails != null;
}

public record Event : EventBase
{
    public Event()
    {
    }

    public Event(EventEntity entity) : base(entity)
    {
        Details = entity.ProtocolDetailsCase switch
        {
            EventEntity.ProtocolDetailsOneofCase.Http => new HttpDetails(entity.Http),
            EventEntity.ProtocolDetailsOneofCase.Dns => new DnsDetails(entity.Dns),
            EventEntity.ProtocolDetailsOneofCase.Dhcp => new DhcpDetails(entity.Dhcp),
            EventEntity.ProtocolDetailsOneofCase.NetMon => new NetMonDetails(entity.NetMon),
            _ => null
        };
    }

    public object? Details { get; init; }

    public T? DetailsAs<T>() where T : EventDetails => Details as T;
}

public record Event<T> : EventBase where T : EventDetails, new()
{
    public Event()
    {

    }

    public Event(EventEntity entity) : base(entity)
    {
        Details = entity.ProtocolDetailsCase switch
        {
            EventEntity.ProtocolDetailsOneofCase.Http => new HttpDetails(entity.Http) as T,
            EventEntity.ProtocolDetailsOneofCase.Dns => new DnsDetails(entity.Dns) as T,
            EventEntity.ProtocolDetailsOneofCase.Dhcp => new DhcpDetails(entity.Dhcp) as T,
            EventEntity.ProtocolDetailsOneofCase.NetMon => new NetMonDetails(entity.NetMon) as T,
            _ => null
        };
    }

    private Event(Event raw) : base(raw)
    {
        Details = raw.Details as T;
    }

    public T? Details { get; init; }

    public static explicit operator Event<T>(Event raw) => new(raw);
}

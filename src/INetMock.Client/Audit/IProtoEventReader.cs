using System;
using System.Threading;
using System.Threading.Tasks;
using Inetmock.Audit.V1;

namespace INetMock.Client.Audit;

public interface IProtoEventReader : IDisposable, IAsyncDisposable
{
    Task<EventEntity?> ReadAsync(CancellationToken token = default);
}

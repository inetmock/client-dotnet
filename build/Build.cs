using NuGet.Versioning;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.CI.GitLab;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Utilities.Collections;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

[CheckBuildProjectConfigurations]
[ShutdownDotNetAfterServerBuild]
class Build : NukeBuild
{
    private const string NuGetSourceName = "GitLab";
    public static int Main() => Execute<Build>(x => x.Test);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    [Solution] readonly Solution? Solution;
    [GitRepository] readonly GitRepository? GitRepository;
    [GitVersion(NoFetch = true, Framework = "net6.0")] readonly GitVersion? GitVersion;


    GitLab? CI => GitLab.Instance;

    AbsolutePath SourceDirectory => RootDirectory / "src";
    AbsolutePath TestsDirectory => RootDirectory / "tests";
    AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

    Target Clean => _ => _
        .Before(Restore)
        .Executes(() =>
        {
            SourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            TestsDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            EnsureCleanDirectory(ArtifactsDirectory);
        });

    Target Restore => _ => _
        .Executes(() =>
        {
            DotNetRestore(s => s
                .SetProjectFile(Solution));
        });

    Target Compile => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            var assemblyVersion = GitVersion?.AssemblySemVer ??
                                  SemanticVersion.Parse(CI?.CommitTag?.TrimStart('v') ?? "0.0.1").ToNormalizedString();
            var informationalVersion = GitVersion?.InformationalVersion ??
                                       SemanticVersion.Parse(CI?.CommitTag?.TrimStart('v') ?? "0.0.1").ToFullString();

            DotNetBuild(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .SetAssemblyVersion(assemblyVersion)
                .SetFileVersion(assemblyVersion)
                .SetInformationalVersion(informationalVersion)
                .EnableNoRestore());
        });

    Target Format => _ => _
        .DependsOn(Restore)
        .Executes(() => DotNet(CI == null ? "format --no-restore" : "format --no-restore --verify-no-changes"));

    Target Test => _ => _
        .DependsOn(Compile, Format)
        .Executes(() => DotNetTest(s => s
            .SetProjectFile(Solution)
            .SetConfiguration(Configuration)
            .EnableNoRestore()
            .EnableNoBuild()
            .EnableProcessLogOutput()));

    Target Pack => _ => _
        .DependsOn(Compile)
        .OnlyWhenStatic(() => CI != null && !string.IsNullOrEmpty(CI.CommitTag))
        .Executes(() => SourceDirectory
            .GlobFiles("**/*.csproj")
            .ForEach(csproj => DotNetPack(s => s
                .SetProject(csproj)
                .SetConfiguration(Configuration)
                .SetOutputDirectory(ArtifactsDirectory)
                .SetVersion(GitVersion?.FullSemVer ?? SemanticVersion.Parse(CI?.CommitTag?.TrimStart('v') ?? "0.0.1").ToNormalizedString())
                .EnableIncludeSource()
                .EnableIncludeSymbols()
                .EnableNoRestore()
                .EnableNoBuild()
                .EnableProcessLogOutput()
            )));

    Target AddNugetSource => _ => _
        .OnlyWhenStatic(() => CI != null && !string.IsNullOrEmpty(CI.CommitTag) && !string.IsNullOrEmpty(CI.JobToken))
        .ProceedAfterFailure()
        .Executes(() => DotNetNuGetAddSource(s => s
            .SetName(NuGetSourceName)
            .SetSource($"https://gitlab.com/api/v4/projects/{CI?.ProjectId}/packages/nuget/index.json")
            .SetUsername("gitlab-ci-token")
            .SetPassword(CI!.JobToken)
            .EnableStorePasswordInClearText()));

    Target NuGetPush => _ => _
        .DependsOn(Pack, AddNugetSource)
        .OnlyWhenStatic(() => CI != null && !string.IsNullOrEmpty(CI.CommitTag))
        .Executes(() => ArtifactsDirectory
            .GlobFiles("**/*.nupkg")
            .ForEach(nupkg => DotNetNuGetPush(s => s
                .SetSource(NuGetSourceName)
                .SetTargetPath(nupkg)
                .EnableProcessLogOutput())));
}

# INetMock .NET client library

## NuGet source

Due to limitations in GitLab one is required to be authenticated to use packages in GitLab NuGet registry.
The following credentials are `read_only` and allowed to be used by anyone.

```shell
dotnet nuget add source \
  https://gitlab.com/api/v4/projects/24385200/packages/nuget/index.json \
  -n GitLabInetMock \
  -u gitlab+deploy-token-776107 \
  -p 3FLWym5gfajuJNhBKvWV \
  --store-password-in-clear-text
```

## Reading audit files

```c#
using System.IO;
using System.Threading.Tasks;
using INetMock.Client.Audit;
using INetMock.Client.Audit.Serialization;

await using var auditFileStream = File.OpenRead("test.ima");
await using IEventReader reader = new GenericReader(new ProtoReader(auditFileStream));

await foreach (var ev in reader.ReadAllAsync())
{
    Console.WriteLine(ev.Application);
}
```
